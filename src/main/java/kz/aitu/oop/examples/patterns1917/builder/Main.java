package kz.aitu.oop.examples.patterns1917.builder;

public class Main {

    public static void main(String[] args) {

        HouseBuilder houseBuilder = new HouseBuilder();

        House house = houseBuilder
                .addDoor()
                .addDoor()
                .addDoor()
                .build();

        System.out.println(house);
    }
}

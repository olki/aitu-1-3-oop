package kz.aitu.oop.examples.patterns1917.builder;

public class HouseBuilder {

    private House house;

    public HouseBuilder() {
        house = new House();
    }

    public HouseBuilder addDoor() {
        house.setDoors(house.getDoors() + 1);
        return this;
    }

    public HouseBuilder addGarden() {
        house.setGarden(true);
        return this;
    }

    public House build() {
        return house;
    }
}

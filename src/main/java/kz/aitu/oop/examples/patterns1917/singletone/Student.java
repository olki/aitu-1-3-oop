package kz.aitu.oop.examples.patterns1917.singletone;

public class Student {

    private String name;
    private static int count = 0;

    private static Student instance = null;

    private Student() {
        count++;
    }

    public static Student getInstance() {
        if(instance == null) instance = new Student();
        return instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int countn) {
        count = countn;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                "count='" + count + '\'' +
                '}';
    }
}

package kz.aitu.oop.examples.patterns1917.singletone;
//bonus points
// static count = Askar
// static count constructor = Juliya
// static method = Dariya
// static instance x2 = Juliya
// static instance mini = Dariya
public class Main {
    public static void main(String[] args) {
        Student.setCount(10);

        Student student1 = Student.getInstance();
        student1.setName("111");

        Student student2 = Student.getInstance();
        student2.setName("222");

        Student student3 = Student.getInstance();
        student3.setName("333");

        Student student4 = Student.getInstance();

        System.out.println(student4.toString());
    }
}

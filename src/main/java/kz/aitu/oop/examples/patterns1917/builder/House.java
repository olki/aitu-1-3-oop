package kz.aitu.oop.examples.patterns1917.builder;

import lombok.Data;

@Data
public class House {

    private int doors;
    private int walls;
    private int windows;
    private boolean garden;
    private boolean swimmingPool;
    private boolean garage;
    private boolean gym;

    @Override
    public String toString() {
        return "House{" +
                "doors=" + doors +
                ", walls=" + walls +
                ", windows=" + windows +
                ", garden=" + garden +
                ", swimmingPool=" + swimmingPool +
                ", garage=" + garage +
                ", gym=" + gym +
                '}';
    }
}

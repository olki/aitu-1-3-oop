package kz.aitu.oop.examples.exceptions;

public class LogicNotFoundException extends Exception {

    private int id;

    public LogicNotFoundException(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "LogicNotFoundException[" + id + "]";
    }
}

package kz.aitu.oop.examples.patterns1914.builder;

public class HouseBuilder {

    private House house;

    public HouseBuilder() {
        house = new House();
    }

    public HouseBuilder addWall() {
        house.setWalls(house.getWalls() + 1);
        return this;
    }

    public HouseBuilder addDoor() {
        house.setDoors(house.getDoors() + 1);
        return this;
    }

    public HouseBuilder addWindow() {
        house.setWindows(house.getWindows() + 1);
        return this;
    }

    public HouseBuilder addGarden() {
        house.setGarage(true);
        return this;
    }

    public HouseBuilder addSwimmingPool() {
        house.setSwimmingPool(true);
        return this;
    }

    public HouseBuilder addGarage() {
        house.setGarage(true);
        return this;
    }

    public HouseBuilder addGym() {
        house.setGym(true);
        return this;
    }


    public House make() {
        return house;
    }

}

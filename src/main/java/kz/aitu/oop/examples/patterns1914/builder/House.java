package kz.aitu.oop.examples.patterns1914.builder;

import lombok.Data;

@Data
public class House {

    private int windows;
    private int doors;
    private int walls;
    private boolean garden;
    private boolean swimmingPool;
    private boolean garage;
    private boolean gym;

    public House() {
        windows = 0;
        doors = 0;
        walls = 0;
    }

    @Override
    public String toString() {
        return "House{" +
                "windows=" + windows +
                ", doors=" + doors +
                ", walls=" + walls +
                ", garden=" + garden +
                ", swimmingPool=" + swimmingPool +
                ", garage=" + garage +
                ", gym=" + gym +
                '}';
    }
}

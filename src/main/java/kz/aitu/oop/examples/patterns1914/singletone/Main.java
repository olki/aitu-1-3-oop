package kz.aitu.oop.examples.patterns1914.singletone;

public class Main {


    public static void main(String[] args) {


        MySystem mySystem1 = MySystem.getInstance();
        MySystem mySystem2 = MySystem.getInstance();
        mySystem2.setName("1");
        MySystem mySystem3 = MySystem.getInstance();


        System.out.println(mySystem1);
        System.out.println(mySystem2);
        System.out.println(mySystem3);
        System.out.println(mySystem1 == mySystem2);
    }
}

package kz.aitu.oop.examples.patterns1914.builder;

public class Main {

    public static void main(String[] args) {

        HouseBuilder houseBuilder = new HouseBuilder();

        House house = houseBuilder
                .addGarden()
                .addDoor()
                .addDoor()
                .addWindow()
                .addWindow()
                .addWindow()
                .addWindow()
                .addWindow()
                .addWindow()
                .addSwimmingPool()
                .make();

        System.out.println(house);
    }
}

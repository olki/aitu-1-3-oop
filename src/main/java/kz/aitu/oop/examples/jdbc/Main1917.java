package kz.aitu.oop.examples.jdbc;

import kz.aitu.oop.entity.Student;

import java.sql.*;

public class Main1917 {


    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/aitu";
        String username = "root";
        String password = "Techno2015";

        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from student");

            while(resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setName(resultSet.getString("name"));
                student.setAge(resultSet.getInt("age"));
                student.setPoint(resultSet.getDouble("point"));

                System.out.println(student.toString());
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

package kz.aitu.oop.examples.polymorphism1917;

public class Account {


    private String creditCard;
    private double balance;

    public Account(String creditCard, double balance) {
        this.creditCard = creditCard;
        this.balance = balance;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public double getBalance() {
        return balance;
    }

    public void addMoney(double money) {
        if(money <= 0) return;

        System.out.println(creditCard + " addMoney " + money);
        balance += money;
    }

    public void removeMoney(double money) {
        if(money <= 0) return;

        System.out.println(creditCard + " removeMoney " + money);
        balance -= money;
    }


}

package kz.aitu.oop.examples.polymorphism1917;

public class Main {


    public static void main(String[] args) {

        PrivilegeAccount privilegeAccount =
                new PrivilegeAccount("142", 10000, 10.5);

        VIPAcount vipAcount =
                new VIPAcount("777", 99999, 5.5);


        Account[] accountList = new Account[4];
        accountList[0] = privilegeAccount;
        accountList[1] = vipAcount;
        accountList[2] = new PrivilegeAccount("001", 556565.0, 15.0);
        accountList[3] = new VIPAcount("002", 556565.0, 15.0);


        for (Account account : accountList) {
            System.out.println("****************");
            System.out.println(account.getCreditCard() + ": " + account.getBalance());
            account.removeMoney(1000);
            System.out.println(account.getCreditCard() + ": " + account.getBalance());

            if(account instanceof VIPAcount) {
                VIPAcount vipAcc = (VIPAcount)account;
                System.out.println("Bonus: " + vipAcc.getBonus());
            }

            if(account instanceof PrivilegeAccount) {
                PrivilegeAccount pAcc = (PrivilegeAccount)account;
                System.out.println("Discount: " + pAcc.getDiscount());
            }
        }
    }
}

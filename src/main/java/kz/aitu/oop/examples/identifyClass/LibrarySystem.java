package kz.aitu.oop.examples.identifyClass;

public class LibrarySystem {

    private Catalog catalog;
    private BorrowerDatabase borrowerDB;

    public LibrarySystem(Catalog catalog, BorrowerDatabase borrowerDB) {
        this.catalog = catalog;
        this.borrowerDB = borrowerDB;
    }

    public void displayCatalog() {
        for (CatalogItem catalogItem : catalog.getItems()) {
            System.out.println(catalogItem);
        }
    }
}

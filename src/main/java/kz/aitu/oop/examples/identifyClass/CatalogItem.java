package kz.aitu.oop.examples.identifyClass;

import lombok.Data;

@Data
public class CatalogItem {

    private String code;
    private String title;
}

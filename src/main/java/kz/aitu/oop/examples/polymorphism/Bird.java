package kz.aitu.oop.examples.polymorphism;

public class Bird extends Animal {


    public Bird(String name) {
        super(name);
    }

    public void fly() {
        System.out.println("The bird is flying....");
    }

    public void fly(int height) {
        System.out.println("The bird is flying " + height + " meter high.");
    }

    public void fly(String name, int height) {
        System.out.println("The " + name + " is flying " + height + " meter high.");
    }


    public void eat() {
        System.out.println("Birds eats....");
    }
}

package kz.aitu.oop.examples.polymorphism1914;

public class Main {


    public static void main(String[] args) {
        Animal animal = new Animal("animal 1");

        System.out.println(animal.getName());
        animal.eat();

        System.out.println("/////////////////");
        Dog dog = new Dog("Dog1");
        dog.run();
        dog.eat();

        System.out.println("///////////////");
        Bird bird = new Bird("Bird1");
        bird.fly();
        bird.fly(5000);
        bird.eat();

        System.out.println("*-*****************");

        Dog[] dogs = new Dog[3];
        dogs[0] = new Dog("dog1");
        dogs[1] = new Dog("dog2");
        dogs[2] = new Dog("dog3");

        for (Dog dogElement : dogs) {
            dogElement.eat();
            dogElement.run();
        }

        Bird[] birds = new Bird[4];
        birds[0] = new Bird("bird1");
        birds[1] = new Bird("bird2");
        birds[2] = new Bird("bird3");
        birds[3] = new Bird("bird4");

        for (Bird birdElement : birds) {
            birdElement.eat();
            birdElement.fly();
        }

        System.out.println("----------------------");
        Animal[] animals = new Animal[7];
        animals[0] = dogs[0];
        animals[1] = birds[0];
        animals[2] = dogs[1];
        animals[3] = birds[1];
        animals[4] = dogs[2];
        animals[5] = birds[3];
        animals[6] = birds[2];

        for (Animal animalElement : animals) {
            animalElement.eat();

            if(animalElement instanceof Dog) {
                Dog dogE = (Dog)animalElement;
                dogE.run();
            }

            if(animalElement instanceof Bird) {
                ((Bird)animalElement).fly(3000);
            }
        }

    }
}
